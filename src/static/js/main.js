const isMobile = () => (window.innerWidth < 900) ? true : false;
const isMerchArea = () => (window.location.pathname === '/merch');

const hideShowToggle = (element) => element.classList.contains('none') ? element.classList.remove('none') : element.classList.add('none');

const changeImageOnHover = (element, imgX, imgY) => {
  element.addEventListener('mouseover', () => {
    element.setAttribute('src', imgX);
  });
  element.addEventListener('mouseleave', () => {
    element.setAttribute('src', imgY);
  });
};

const closeMenu = (icon, menu) => {
  if(icon.classList.contains('open')) {
    icon.classList.remove('open');
  }

  if(menu.classList.contains('open')) {
    menu.classList.remove('open');
  }
};

//FOOTER
//ACCORDIONS
let legalArea = document.getElementById('legalarea'),
  legalAreaDescription = document.querySelector('.main__footer__accordion__inner__legalarea'),
  followUs = document.getElementById('followus'),
  socialList = document.querySelector('.main__footer__accordion__inner__followus'),
  ourMusic = document.getElementById('ourmusic'),
  musicPlatformList = document.querySelector('.main__footer__accordion__inner__ourmusic');

if(isMerchArea()){
  var firstItem = document.querySelector('.first__item__img');
}

//FOLLOW US
followUs.addEventListener('click', function (evt) {
  evt.preventDefault();
  if (isMobile()) {
    hideShowToggle(socialList);
  } else return;
});

//OUR MUSIC
ourMusic.addEventListener('click', function (evt) {
  evt.preventDefault();
  if (isMobile()) {
    hideShowToggle(musicPlatformList);
  } else return;
});

//LEGAL AREA
legalArea.addEventListener('click', function (evt) {
  evt.preventDefault();
  if (isMobile()) {
    hideShowToggle(legalAreaDescription);
  } else return;
});

//click handler -> menu open/close
let burgerMenuIcon,
  burgerMenuList;

burgerMenuIcon = document.querySelector('.main__burger');
burgerMenuList = document.querySelector('.main__navigation');

burgerMenuIcon.addEventListener('click', (evt) => {
  evt.target.classList.toggle('open');
  burgerMenuList.classList.toggle('open');
});

burgerMenuList.addEventListener('focusout', () => closeMenu(burgerMenuIcon, burgerMenuList));

// footer responsivness
window.onresize = function () {
  if (!isMobile()) {
    closeMenu(burgerMenuIcon, burgerMenuList);
    legalAreaDescription.classList.remove('none');
    socialList.classList.remove('none');
    musicPlatformList.classList.remove('none');
    if(isMerchArea()){
      changeImageOnHover(firstItem, 'images/y_detachment_tshirt.jpg', 'images/x_detachment_tshirt.jpg');
    }
  } else {
    if (!legalAreaDescription.classList.contains('none')) {
      legalAreaDescription.classList.add('none');
    }
    if (!socialList.classList.contains('none')) {
      socialList.classList.add('none');
    }
    if (!musicPlatformList.classList.contains('none')) {
      musicPlatformList.classList.add('none');
    }
    if(isMerchArea()){
      changeImageOnHover(firstItem, 'images/x_detachment_tshirt.jpg', 'images/x_detachment_tshirt.jpg');
    }
  }
};

window.onload = function () {
  if (!isMobile()) {
    legalAreaDescription.classList.remove('none');
    socialList.classList.remove('none');
    musicPlatformList.classList.remove('none');
    if(isMerchArea()){
      changeImageOnHover(firstItem, 'images/y_detachment_tshirt.jpg', 'images/x_detachment_tshirt.jpg');
    }
  }
};
