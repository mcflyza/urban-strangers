import {
  src,
  dest
} from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import sass from 'gulp-dart-sass';
import cleanCSS from 'gulp-clean-css';
import tasksPaths from '../utils/tasksPaths';
import infoCompilingScss from '../utils/infoCompilingScss';

const cleanCssOptions = {
  level: {
    1: {
      all: true,
      sourcemaps: true
    },
    2: {
      all: true,
    }
  }
};

const compileScss = () => src(tasksPaths.srcScss)
  .pipe(sourcemaps.init())
  .pipe(sass.sync().on('error', sass.logError))
  .pipe(cleanCSS(cleanCssOptions, (details) => infoCompilingScss(details)))
  .pipe(sourcemaps.write('/maps'))
  .pipe(dest(tasksPaths.destScss));

export default compileScss;
