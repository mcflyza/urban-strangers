import { src } from 'gulp';
import sassLint from 'gulp-sass-lint';
import tasksPaths from '../utils/tasksPaths';

const lintScss = () => src(tasksPaths.srcScss)
  .pipe(sassLint())
  .pipe(sassLint.format())
  .pipe(sassLint.failOnError());

export default lintScss;
