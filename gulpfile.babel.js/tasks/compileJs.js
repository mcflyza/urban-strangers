import {
  src,
  dest
} from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import minify from 'gulp-babel-minify';
import babel from 'gulp-babel';
import tasksPaths from '../utils/tasksPaths';

const compileJs = () => src(tasksPaths.srcJs)
  .pipe(sourcemaps.init())
  .pipe(babel())
  .pipe(minify())
  .pipe(sourcemaps.write('/maps'), {addComment: false})
  .pipe(dest(tasksPaths.destJs));

export default compileJs;
