import { src } from 'gulp';
import eslint from 'gulp-eslint';
import tasksPaths from '../utils/tasksPaths';

const lintJs = () => src([tasksPaths.srcJs, tasksPaths.srcServer])
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());

export default lintJs;
