import {
  src,
  dest
} from 'gulp';

const copy = (source, destination) => src(source)
  .pipe(dest(destination));

export default copy;