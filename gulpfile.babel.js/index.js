import {
  task,
  watch,
  series,
  parallel
} from 'gulp';
import tasksPaths from './utils/tasksPaths';
import compileScss from './tasks/compileScss';
import lintScss from './tasks/lintScss';
import compileJs from './tasks/compileJs';
import lintJs from './tasks/lintJs';
import copy from './tasks/copy';

// **styles**
task('lint-scss', lintScss);
task('compile-scss', compileScss);
task('build-scss', series('lint-scss', 'compile-scss'));

// **scripts**
task('lint-js', lintJs);
task('compile-js', compileJs);
task('build-js', series('lint-js', 'compile-js'));

// **views**
task('copy-hbs', () => copy(tasksPaths.srcHbs, tasksPaths.destHbs));

// **static images**
task('copy-image', () => copy(tasksPaths.srcStaticImage, tasksPaths.destStaticImage));

// **sitemap**
task('copy-stmp', () => copy(tasksPaths.srcStmp, tasksPaths.destStmp));

// ** copy **
task('copy', series('copy-hbs', 'copy-image', 'copy-stmp'));

// **builders**
task('build-machine', series('build-scss', 'build-js', 'copy'));

// **wathcers**
task('watch:scss', () => watch(tasksPaths.srcScss, series('build-scss', 'copy')));
task('watch:js', () => watch(tasksPaths.srcJs, series('build-js', 'copy')));
task('watch', parallel('watch:scss', 'watch:js'));
