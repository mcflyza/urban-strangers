import formatBytes from './formatBytes';

/* eslint-disable no-console */
const infoCompilingScss = (details) => {
  console.log(`${details.name}`);
  console.log(`original size: ${formatBytes(details.stats.originalSize, 2)}`);
  console.log(`minified size: ${formatBytes(details.stats.minifiedSize, 2)}`);
};

export default infoCompilingScss;
