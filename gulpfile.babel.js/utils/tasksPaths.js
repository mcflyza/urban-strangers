import path from 'path';

// init config obj
const tasksPaths = {};

let src = path.join(__dirname, '../../src/');
let dest = path.join(__dirname, '../../dist/');

let srcAssets = path.join(src, '/static/');
let destAssets = path.join(dest, '/assets/');

let srcViews = path.join(src, '/views');
let destViews = path.join(dest, '/views');

// SCSS files
tasksPaths.srcScss = path.join(srcAssets, '/scss/**/*.scss');
tasksPaths.destScss = path.join(destAssets, '/styles/');

// JS files
tasksPaths.srcJs = path.join(srcAssets, '/js/**/*.js');
tasksPaths.destJs = path.join(destAssets, '/scripts/');

// images
tasksPaths.srcStaticImage = path.join(srcAssets, '/images/**');
tasksPaths.destStaticImage = path.join(destAssets, '/images/');

// HBS views
tasksPaths.srcHbs = path.join(srcViews, '/**');
tasksPaths.destHbs = destViews;

// sitemap
tasksPaths.srcStmp = path.join(srcAssets, '/sitemap.xml');
tasksPaths.destStmp = destAssets;

// server files
tasksPaths.srcServer = path.join(__dirname, '../../server/**/*.js');

export default tasksPaths;
