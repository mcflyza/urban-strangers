var path = require('path');

// init config obj
const routesPath = {};

// views
routesPath.views = path.join(__dirname + './../../dist/views/');

//layout
routesPath.layout = path.join(routesPath.views, '/layout');

//partials
routesPath.partials = path.join(routesPath.views, '/shared');

module.exports = routesPath;