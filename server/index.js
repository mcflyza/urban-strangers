// builtin
var path = require('path'),
  express = require('express'),
  hbs = require('hbs'),
  routesPath = require('./utils/routesPath'),
  routes = require('./routes/routes');

// create a middleware
var app = express();

// set the view engine to use handlebars
app.set('view engine', 'hbs');
app.set('views', routesPath.layout);

hbs.registerPartials(routesPath.partials);
hbs.registerPartials(routesPath.layout);

app.use(express.static(path.join(__dirname + './../dist/assets')));

// use my routes
app.use(routes);

//server data
const hostname = '127.0.0.1',
  port = process.env.PORT || 8180;

app.listen(port, () =>
  /* eslint-disable no-console */
  console.log(`Magic happens at http://${hostname}:${port}`)
);
