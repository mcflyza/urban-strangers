const router = require('express').Router(),
  menuItem = require('./info/shared/menu'),
  videos = require('./info/videos/videos'),
  footer = require('./info/shared/footer');

router.get('/video', function(req, res) {
  res.locals = {
    title: "Urban Strangers",
    menuItem: menuItem,
    videos: videos,
    footer: footer
  };
  res.render('video');
});

module.exports = router;