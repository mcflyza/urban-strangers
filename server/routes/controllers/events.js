const router = require('express').Router(),
  menuItem = require('./info/shared/menu'),
  events = require('./info/events/events'),
  footer = require('./info/shared/footer');

router.get('/events', function(req, res) {
  res.locals = {
    title: "Urban Strangers",
    menuItem: menuItem,
    events: events,
    footer: footer
  };
  res.render('events');
});

module.exports = router;
