var albums = {
  1: {
    title: 'U.S.',
    songs: {
      1: {
        name: 'Non so',
        link: 'https://open.spotify.com/track/6bEbak9vgNQ3qWgufeWgNy'
      },
      2: {
        name: 'Lasciare andare',
        link: 'https://open.spotify.com/track/3spSEWiBVqYATz32ZUWBEV'
      },
      3: {
        name: 'I sensi e le colpe',
        link: 'https://open.spotify.com/track/3dv1YP7Ka1lu3ptnSZBnl5'
      },
      4: {
        name: 'Unico ricordo',
        link: 'https://open.spotify.com/track/5KY3S1E4jsKobxtbdgZYZd'
      },
      5: {
        name: 'Stare calmo',
        link: 'https://open.spotify.com/track/35hdrBate8UOPBZrO4W8IC'
      },
      6: {
        name: 'Nel mio giorno migliore',
        link: 'https://open.spotify.com/track/1lLoFgTB5RFr710vxYJphE'
      },
      7: {
        name: 'Non andrò via',
        link: 'https://open.spotify.com/track/4OHJg4dEpTuDd0UoZ5VMGF'
      },
      8: {
        name: 'Sono io?',
        link: 'https://open.spotify.com/track/4OHJg4dEpTuDd0UoZ5VMGF'
      },
      9: {
        name: 'Crisi astronomiche',
        link: 'https://open.spotify.com/track/6vFZNkNT72nJRihQulH5FV'
      },
      10: {
        name: 'Tutto finisce',
        link: 'https://open.spotify.com/track/6XheCIYWxvAxuWTyvE61sv'
      }
    }
  },
  2: {
    title: 'Detachment',
    songs: {
      1: {
        name: 'No electric',
        link: 'https://open.spotify.com/track/672GfKwM0IYeiVnce380T5'
      },
      2: {
        name: 'Stronger',
        link: 'https://open.spotify.com/track/6oIMEBBJkZrtvk3fNlgAEF'
      },
      3: {
        name: 'Bones',
        link: 'https://open.spotify.com/track/2JehlcrVgGast3QkEvcR9Y'
      },
      4: {
        name: 'My fault',
        link: 'https://open.spotify.com/track/5QpjWbkpIos7C3aApIaIsH'
      },
      5: {
        name: '5',
        link: 'https://open.spotify.com/track/6yfCnQijAHRecM8nNEaYfu'
      },
      6: {
        name: 'Warrior',
        link: 'https://open.spotify.com/track/0QHO9pkejbS6IncWbWuPo8'
      },
      7: {
        name: 'Leaf',
        link: 'https://open.spotify.com/track/6BpsEGAdATJWlvxCjAmTmv'
      },
      8: {
        name: 'Bare Black Tree',
        link: 'https://open.spotify.com/track/3xwQwyVYbnZml35wnDAcRO'
      },
      9: {
        name: 'So',
        link: 'https://open.spotify.com/track/1KDem4DsL6eMByRSrfGTzl'
      },
      10: {
        name: 'Rising',
        link: 'https://open.spotify.com/track/48qXiJ5TgaPjvY8y1iJa70'
      },
      11: {
        name: 'Medical',
        link: 'https://open.spotify.com/track/2FOq1iUDxKoqVjGKYeWKfq'
      },
      12: {
        name: 'Intro',
        link: 'https://open.spotify.com/track/18jtrU0vHLzqy9XaCMZZ6k'
      }
    }
  }
};

module.exports = albums;