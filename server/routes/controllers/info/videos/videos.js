var videos = {
  1: {
    title: 'Non andro\' via',
    link: 'https://www.youtube.com/embed/rhS8BAO0yMk'
  },
  2: {
    title: 'Non so',
    link: 'https://www.youtube.com/embed/NZyRKVYEH5A'
  },
  3: {
    title: 'Stronger',
    link: 'https://www.youtube.com/embed/EDjDR3Ylq40'
  },
  4: {
    title: 'Bones',
    link: 'https://www.youtube.com/embed/s_Zm_0ZODxE'
  },
  5: {
    title: 'Last Part',
    link: 'https://www.youtube.com/embed/9vSPOBL5l_k'
  },
  6: {
    title: 'Runaway',
    link: 'https://www.youtube.com/embed/9xkqUUs3KbQ'
  },
  7: {
    title: 'Empty Bed',
    link: 'https://www.youtube.com/embed/IvXWFV3h4sw'
  }
};

module.exports = videos;
