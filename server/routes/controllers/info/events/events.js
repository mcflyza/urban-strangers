var events = {
  1: {
    link: "https://www.facebook.com/events/766121553750948/",
    description: "24 Gennaio @ Duel Club, Napoli"
  },
  2: {
    link: "https://www.facebook.com/events/291808138184457/",
    description: "16 Febbraio @ Periferica Konnection, Fisciano (SA)"
  },
  3: {
    link: "https://www.facebook.com/events/766121553750948/",
    description: "21 Marzo @ The Yellow Bar, Roma"
  },
  4: {
    link: "https://www.facebook.com/events/1564605163684153/",
    description: "22 Marzo @ Off Bologna, Bologna"
  },
  5: {
    link: "https://www.facebook.com/events/2265917560097809/",
    description: "23 Marzo @ Magazzino sul Po, Torino"
  }
};

module.exports = events;