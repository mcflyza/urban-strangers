var footer = {
  followUs: {
    facebook: {
      link: 'https://www.facebook.com/UrbanStrangersOfficial',
      pathImg: 'images/icon/facebook.svg',
      txt: 'Facebook'
    },
    instagram: {
      link: 'https://www.instagram.com/urbanstrangers',
      pathImg: 'images/icon/instagram.svg',
      txt: 'Instagram'
    },
    twitter: {
      link: 'https://twitter.com/urban_strangers',
      pathImg: 'images/icon/twitter.svg',
      txt: 'Twitter'
    }
  },
  ourMusic: {
    spotify: {
      link: 'https://open.spotify.com/artist/21vnkDMaPE9lABjqptWisA',
      pathImg: 'images/icon/spotify.svg',
      txt: 'Spotify'
    },
    tidal: {
      link: 'https://tidal.com/browse/artist/6691587',
      pathImg: 'images/icon/tidal.svg',
      txt: 'Tidal'
    },
    appleMusic: {
      link: 'https://itunes.apple.com/it/artist/urban-strangers/973640925',
      pathImg: 'images/icon/apple-music.svg',
      txt: 'Apple Music'
    }
  }
};

module.exports = footer;