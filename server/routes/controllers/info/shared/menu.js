var menuItem = {
  1: {
    route: '/events',
    name: 'events'
  },
  2: {
    route: '/biography',
    name: 'biography'
  },
  3: {
    route: '/merch',
    name: 'merch'
  },
  4: {
    route: '/video',
    name: 'video'
  },
  5: {
    route: '/discography',
    name: 'discography'
  }
};

module.exports = menuItem;