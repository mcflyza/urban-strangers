const router = require('express').Router(),
  menuItem = require('./info/shared/menu'),
  footer = require('./info/shared/footer');

router.get('/merch', function(req, res) {
  res.locals = {
    title: "Urban Strangers",
    menuItem: menuItem,
    footer: footer
  };
  res.render('first_item');
});

module.exports = router;
