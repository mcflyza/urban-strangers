const router = require('express').Router(),
  menuItem = require('./info/shared/menu'),
  albums = require('./info/music/albums'),
  infoMusicArticle = require('./info/music/infoMusicArticle'),
  footer = require('./info/shared/footer');

router.get('/discography', function(req, res) {
  res.locals = {
    title: "Urban Strangers",
    menuItem: menuItem,
    albums: albums,
    infoMusicArticle: infoMusicArticle,
    footer: footer
  };
  res.render('discography');
});

module.exports = router;
