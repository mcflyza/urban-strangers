const index = require('./controllers/index'),
  merch = require('./controllers/merch'),
  events = require('./controllers/events'),
  biography = require('./controllers/biography'),
  video = require('./controllers/video'),
  discography = require('./controllers/discography'),
  pageNotFound = require('./controllers/page-not-found');

//array of routes
let routes = [
  index,
  merch,
  events,
  biography,
  video,
  discography,
  pageNotFound
];

module.exports = routes;
